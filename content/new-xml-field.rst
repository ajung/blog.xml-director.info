Moving forward with Plone and eXist-db integration 
##################################################

:date: 2014-12-08 17:20
:tags: xml-director, xml
:category: blog
:slug: dexterity-new-fields
:authors: Andreas Jung

We decided to integrate eXist-db much tigher with Plone as initially planned
(for strategic reasons). After heaving fighting with z3c.form I finally got to
the point where I have now 4 new different Dexterity fields that store their
data in eXist-db instead of the ZODB.

- XMLText: this field holds an arbitrary XML document. XML validation is
  performed both client-side and server-side. The widget for XMLText is the ACE
  editor.
- XMLXPath: this field can be used to pull in parts of an content from an
  arbitrary XMLText field by specifying the name of the XMLText field and an
  arbitrary XPath expression
- XMLImage: as as the Dexerity image field but with eXist-db as storage
- XMLBinary: same as the Dexterity file field but with eXist-db as storage
