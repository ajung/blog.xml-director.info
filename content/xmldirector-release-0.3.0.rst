First release of xmldirector.plonecore 0.3.0
############################################

:date: 2014-12-11 09:00
:tags: xml-director, xml
:category: blog
:slug: xmldirector.plonecore
:authors: Andreas Jung

I released today the first release of the XML-Director foundation
xmldirector.plonecore.

This module brings XML databases like eXist-db or Base-X into the
CMS Plone:

- support for mounting a complete tree from eXist-db or Base-X into Plone

- support for Plone content-types with fields for XML content, images and files
  where the data is persisted in either eXist-db or Base-X

- XML-ish content-types can be modeled within Plone either through-the-web
  or programmtically (using Python and the underlaying framework Dexterity
  for specifying content-types)

As an implementation side-effect this release also integrates Alfresco
or OwnCloud with Plone - however this is not the primary goal of the project.

Technical aspects of this release:

- moving forward 100% test coverage of all code
- continuous integration with Travis-CI
- code coverage reports through coveralls.io (coming up)
- update-to-date documentation


