#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'ZOPYX'
SITENAME = u'XML-Director Blog'
SITEURL = ''

# Tell Pelican to add 'extra/custom.css' to the output dir
STATIC_PATHS = ['images', 'extra/custom.css']
CUSTOM_CSS = 'static/custom.css'
EXTRA_PATH_METADATA = {
            'extra/custom.css': {'path': 'static/custom.css'}
            }

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


DEFAULT_PAGINATION = 10


LINKS = (('XML-Director', 'http://xml-director.info/'),
    ('XML-Director Blog', 'http://blog.xml-director.info/'),
    ('Code @Github', 'https://github.com/xml-director'),
    ('ZOPYX', 'http://www.zopyx.com'),
    ('Contact', 'mailto:info@zopyx.com'))
 
 # Social widget
SOCIAL = (('Facebook', 'http://facebook.com/xmldirector'),
          ('Twitter', 'http://twitter.com/xmldirector'),)

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'pelican-bootstrap3'
THEME = 'elegant'
THEME = 'svbtle'

DATE_FORMATS = {
    'en': '%d.%m.%Y',
}

SITELOGO = 'images/logo.png'

GOOGLE_ANALYTICS = 'UA-65716-18'
